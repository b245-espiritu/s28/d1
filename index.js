// CRUD Operations
    // CRUD Operation is the heart of any backend application
    // Masterning the CRUD operations is essential for any developer
    // This helps in building character and increasing exposure to logical statements that will help us manipulate.
    // Mastering CRUD operations of any launguages make us valuable developer and makes the work easier for us to deal with huge of information.


    // Create(Inserting document);
    // since MongoDB deals with object as it's structure for documents, we can easily create them by providing objects into our methods.
    // mongDB shell also users javascript for it's syntax which makes it convenients for us to understand it's code.


    //Syntax
    // db.colletionName.insertOne({Object/document})   -- one object only
    


    db.users.insertOne(
        {
            firstName: 'Jane',
            lastName: 'Doe',
            age: 21,
            contact: {
                phone: '09157230777',
                email: 'espirituleoncio@gmail.com'
            },
            course: ['CSS', 'Javascript', 'Python'],
            department: 'none'
        }
    );


    //Insert Many
    /* 
    
    db.users.insertMany([ {} {}])

    array of object
    */

    db.users.insertMany(
        [
            {
                firstName: 'Stephen',
                lastName: 'Hawking',
                age: 76,
                contact: {
                    phone: '87654321',
                    email: 'stephenhawking@gmail.com'
                },
                course: ['Phyton', 'React', 'PHP'],
                department: 'none'
            },

            {
                firstName: 'Neil',
                lastName: 'Armstrong',
                age: 32,
                contact: {
                    phone: '87654321',
                    email: 'neilarmstrong@gmail.com'
                },
                course: ['Phyton', 'Laravel', 'Sass'],
                department: 'none'
            }
        ]
    );


    // Find (read)
    /*

    it will show us all the documents in our collection

     Syntax:
        db.collectionName.find();
      

     */


        // finding all
        db.users.find();

        //finding with specific fieldname
        db.users.find({age:76});
        db.users.find({firstName:'jane'});

        // find documents using multiple fieldsets

        /*
            syntax:
            db.collectionName.find({
                fieldA:valueA,
                fieldB:valueB
            })
        */

            db.users.find({
                lastName:'armstrong',
                age: 32
            });

    




    
            // just adding another user
            db.users.insertOne({
                firstName:'test',
                lastName:'test',
                age:1,
                contact:{
                    phone:'03021654',
                    email:'test@gmail.com'
                },
                course:[],
                department:'none'
            });


            
            
        //Updating Documents

        /*
            Syntax:

            db.collectionName.updateOne({criteria},
                {
                    $set:{
                        
                    }
                })
        */

                db.users.updateOne({firstName: 'Jane'},
                {
                    $set:{
                        lastName:'Hawking'
                    }
                }
                );


      //Updating Many documents          
                db.users.updateMany( {firstName:'Jane'},{
                    $set: {
                        lastName:'Wick',
                        age:'25'
                    }
                });

                db.users.updateMany({department:'none'},{
                    $set:{
                        department:'HR'
                    }
                });

    // replace the old document or object

    
    /*
        syntax:
        db.users.replaceOne( {criteria}, {document/objectToReplace})
    */


    db.users.replaceOne( {firstName:'test'}, {
        firstName:'Chris',
        lastName:'Mortel'
    });



    //Deleting Documents

    /*
        syntax
        db.users.deleteOne( {criteria})
    */

        db.users.deleteOne({firstName:'Jane'});

        //delete many
        db.users.deleteMany({firstName:'Jane'});

    


        // query on an embeded document


        db.users.find( {course: ['react', 'sass', 'laravel']});

        db.users.find( {course: {$all: ['react']}})